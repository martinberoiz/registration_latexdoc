import numpy as np
import matplotlib.pyplot as plt
import aplpy
from astropy.io import fits
import toros_copy
from toros_copy.instrument import cstar as telescope
import toros_copy.registration as register
from scipy.spatial import KDTree
import os
import argparse

_OUT_DIR = '.'

def plotSources(ref_srcs, test_srcs, output_file_name = None):
    f, (ax1, ax2) = plt.subplots(1,2)
    f.set_size_inches((20,10))
    f.set_tight_layout(True)
    
    #(xmin, ymin), (xmax, ymax) = [map(func, np.hstack((ref_srcs.T, test_srcs.T))) for func in (min, max)]
    
    ax1.scatter(ref_srcs[:,0], ref_srcs[:,1], marker='*', s=220, c='y')
    ax2.scatter(test_srcs[:,0], test_srcs[:,1], marker='*', s=220, c='y')
    
    #dx = (xmax - xmin)*0.1
    #dy = (ymax - ymin)*0.1
    for ax in ax1, ax2:
        #ax.set_xlim((xmin - dx, xmax + dx))
        #ax.set_ylim((ymin - dy, ymax + dy))
        ax.set_aspect('equal')
    
    if output_file_name is not None:
        f.savefig(output_file_name)
    return


def plotInvariantPoints(ref_inv, test_inv, output_file_name = None):
    f = plt.figure(figsize=(10,10))
    f.set_tight_layout(True)
    plt.scatter(ref_inv[:,0],ref_inv[:,1],c='b', marker='o', s=100)
    plt.scatter(test_inv[:,0],test_inv[:,1],c='r',marker = '.', s=100)
    plt.title("Invariants", fontsize=16)
    plt.xlabel("$L_2/L_1$", fontsize=16)
    plt.ylabel("$L_1/L_0$", fontsize=16)
    
    xmax,ymax = map(max, test_inv_uniq.T)
    plt.ylim((0.9, ymax*1.1))
    plt.xlim((0.9, xmax*1.1))
    x = np.linspace(1.01,2.,100)
    plt.plot(x,1./(x-1.))

    (xmin, xmax), (ymin, ymax) = plt.xlim(), plt.ylim()
    ax = plt.subplot(111)
    #x = 1 line
    ax.add_line(plt.Line2D([1.0,1.0],[ymin,ymax], lw=1., ls='--', color='gray'))
    #y = 1 line
    ax.add_line(plt.Line2D([xmin,xmax],[1.0,1.0], lw=1., ls='--', color='gray'))

    if output_file_name is not None:
        plt.savefig(output_file_name)
    return


def plotTransformedSources(ref_srcs, test_srcs, bestTransf, output_file_name = None):
    
    test_srcs_rot = np.hstack((ref_srcs, np.ones((len(ref_srcs),1)))).dot(bestM.T)

    f = plt.figure(figsize=(10,10))
    f.set_tight_layout(True)
    ax = plt.subplot(111)

    ax.scatter(test_srcs[:,0], test_srcs[:,1], marker='*', c='yellow', s=180, zorder=10)
    ax.scatter(test_srcs_rot[:,0], test_srcs_rot[:,1], marker='o', c='red', s=120, zorder=9)
    ax.set_aspect('equal')
    if output_file_name is not None:
        plt.savefig(output_file_name)
    return

    
def plotAstroImageSources(fits_file_name, sources = None, output_full_path = None):
    
    f = plt.figure(figsize=(10,10))
    f.set_tight_layout(True)
    scidata = fits.getdata(fits_file_name)
    scidata[scidata < 0] = scidata[scidata > 0].min()
    f1 = aplpy.FITSFigure(scidata, figure=f)
    f1.axis_labels.hide()
    f1.tick_labels.hide()
    f1.ticks.hide()
    f1.set_theme('publication')
    
    if sources is not None:
        for (x,y) in sources:
            f1.show_markers(x, y, edgecolor='green', facecolor='none',
                            marker='o', s=150)
    f1.show_grayscale()
    if output_full_path is not None:
        plt.savefig(output_full_path)
    return


def plotInvariantMaps(output_file_name = None):
    Li = 1.
    inv = [[],[],[],[]]
    eps = 0.05
    for Lj in np.linspace(eps, Li/2 - eps, 100):
        for alpha in np.linspace(0., np.pi, 100):
            Lk = np.sqrt(Li**2 + Lj**2 - 2.*Li*Lj*np.cos(alpha))
            sides = np.sort([Li,Lj,Lk])
            inv[0].append([sides[2]/sides[1],sides[1]/sides[0]])
            inv[1].append([sides[2]/sides[1],sides[0]/sides[1]])
            #inv[2].append([sides[2]/sides[0],sides[1]/sides[0]])
            inv[2].append([sides[1]/sides[2],sides[0]/sides[2]])
            inv[3].append([sides[2]**2/(sides[0]**2 + sides[1]**2),sides[1]/sides[0]])
    inv = [np.array(anInv) for anInv in inv]

    f, axes = plt.subplots(2,2)
    f.set_tight_layout(True)
    f.suptitle("Several Invariant mappings", fontsize=16)
    f.set_size_inches((10,10))

    axes[0,0].scatter(inv[0][:,0], inv[0][:,1], c='b', marker='.')
    axes[0,0].set_xlabel("$L_2/L_1$", fontsize=16)
    axes[0,0].set_ylabel("$L_1/L_0$", fontsize=16)

    axes[0,1].scatter(inv[1][:,0], inv[1][:,1], c='b', marker='.')
    axes[0,1].set_xlabel("$L_2/L_1$", fontsize=16)
    axes[0,1].set_ylabel("$L_0/L_1$", fontsize=16)

    axes[1,0].scatter(inv[2][:,0], inv[2][:,1], c='b', marker='.')
    axes[1,0].set_xlabel("$L_1/L_2$", fontsize=16)
    axes[1,0].set_ylabel("$L_0/L_2$", fontsize=16)

    axes[1,1].scatter(inv[3][:,0], inv[3][:,1], c='b', marker='.')
    axes[1,1].set_xlabel("$\\frac{L_2^2}{L_0^2 + L_1^2}$", fontsize=16)
    axes[1,1].set_ylabel("$L_1/L_0$", fontsize=16)

    if output_file_name is not None:
        plt.savefig(output_file_name)
    return


def plotInvariantMap01(output_file_name = None):
    f = plt.figure(figsize=(10,10))
    f.set_tight_layout(True)
    ax = plt.subplot(1,1,1)

    xmin, xmax = 0.5, 2.5
    ymin, ymax = 0.0, 5.
    ax.set_xlim((xmin,xmax))
    ax.set_ylim((ymin,ymax))

    #thin isosceles
    ax.add_line(plt.Line2D([1.0,1.0],[1.,ymax], lw=3., ls='-', color='r'))
    #fat isosceles line
    ax.add_line(plt.Line2D([1.,2.],[1.0,1.0], lw=3., ls='-', color='r'))

    #x = 1 line
    ax.add_line(plt.Line2D([1.0,1.0],[ymin,ymax], lw=1., ls='--', color='gray'))
    #y = 1 line
    ax.add_line(plt.Line2D([xmin,xmax],[1.0,1.0], lw=1., ls='--', color='gray'))

    x1 = np.linspace(1., xmax, 100)
    x2 = np.linspace(1., 2., 100)
    ax.plot(x1,1./(x1-1.), lw=1., ls='-', color='gray')
    ax.plot(x2,1./(x2-1.), lw=3., ls='-', color='blue')

    ax.fill_between(x2, 1., 1./(x2 - 1.), color=(0.6,0.8,0.6), hatch='/')
    #ax.fill_between(x, 0., 1./(x - 1.), alpha=0.2, color='g', hatch='/')
    #ax.fill_betweenx(np.linspace(ymin, ymax, 100), 1., xmax, alpha=0.2, color='b', hatch='\\')
    #ax.fill_between(x, 1., ymax, alpha=0.5, color='y', hatch='|')

    ax.set_xlabel('$L_2/L_1$', fontsize=16)
    ax.set_ylabel('$L_1/L_0$', fontsize=16)

    ax.annotate("Equilateral\ntriangles", xy=[1.,1.], xycoords='data', 
                xytext=(0.1,0.1), textcoords='axes fraction',
                arrowprops={'facecolor':'black', 'shrink':0.2},
                horizontalalignment='center', verticalalignment='bottom',
                fontsize=13)

    ax.annotate("Isosceles triangles", xy=(1.,3.), xycoords='data', 
                horizontalalignment='center', verticalalignment='top',
                rotation='vertical', xytext=(-10., 0.), textcoords='offset points',
                fontsize=13)

    ax.annotate("Isosceles triangles", xy=(1.5,1.), xycoords='data', 
                horizontalalignment='center', verticalalignment='top',
                fontsize=12)

    x1 = 1.3
    y1 = 1./(x1 - 1.)
    rot_angle = np.arctan(-1./(x1-1.)**2) * 180. / np.pi
    trans_angle = plt.gca().transData.transform_angles(np.array((rot_angle,)),
                                                       np.array((x1,y1)).reshape((1,2)))[0]

    ax.annotate("Collinear points curve", xy = (x1,y1), xycoords='data', 
                fontsize=12, xytext = (10.,0.), textcoords='offset points',
                horizontalalignment='center', verticalalignment='center',
                rotation = trans_angle)

    ax.scatter([1.],[1.], c='yellow', marker='*',s=500, zorder=10)
    if output_file_name is not None:
        plt.savefig(output_file_name)
    return


if __name__ == '__main__':
    #Parse the command line arguments
    parser = argparse.ArgumentParser(description='Create the figures for a latex document.')
    parser.add_argument('-o', 
                        dest='_OUT_DIR', 
                        metavar='output_dir', 
                        help='The output directory on which to save the figures.',
                        default='.') 
    args = parser.parse_args()
    _OUT_DIR = args._OUT_DIR
       
    ref_srcs = np.array([[9.,8.],[7.1,10.2],[3.,5.],[7.,5.],[4.,1.],[3.,7.],[4.2,9.4],[1.5,10.2],[1.5,4.3],[7.3,3.1]])
    ref_srcs *= 8

    alpha = 30*np.pi/180.
    c = np.cos(alpha)
    s = np.sin(alpha)
    tx = -15.
    ty = 20.
    Mrot = np.array([[c,s,tx],[-s,c,ty]])

    test_srcs = np.hstack((ref_srcs, np.ones((len(ref_srcs),1)))).dot(Mrot.T)

    plotSources(ref_srcs, test_srcs, os.path.join(_OUT_DIR, "idealSources.pdf"))
    
    invMap = register.InvariantTriangleMapping()
    ref_inv_uniq, ref_triang_uniq = invMap.generateInvariants(ref_srcs)
    test_inv_uniq, test_triang_uniq = invMap.generateInvariants(test_srcs)

    plotInvariantPoints(ref_inv_uniq, test_inv_uniq, os.path.join(_OUT_DIR, "idealInvariants.pdf"))
    
    bestM = register.findAffineTransform(test_srcs, ref_srcs)
    plotTransformedSources(ref_srcs, test_srcs, bestM, os.path.join(_OUT_DIR, "idealMatchedSources.pdf"))
    
    #Test now with real data
    
    #ref_file_name = "cstar_ref_image.fits"
    ref_file_name = "master2010.fits"
    refimg = fits.getdata(ref_file_name)
    #refmsk = fits.getdata(ref_file_name, 1).astype('bool')
    refmsk = refimg < 0
    refimg = np.ma.array(refimg, mask = refmsk)
    ref_srcs = toros_copy.skygoldmaster.findSources(refimg)[:70]

    test_file_name = "cstar_test_image.fits"
    testimg = telescope.reduce(test_file_name)
    test_srcs = toros_copy.skygoldmaster.findSources(testimg)[:50]
    
    plotSources(ref_srcs, test_srcs, os.path.join(_OUT_DIR, "realSources.pdf"))
    
    plotAstroImageSources(ref_file_name, ref_srcs, os.path.join(_OUT_DIR, 'realRefImgWithSources.pdf'))
    plotAstroImageSources(test_file_name, test_srcs, os.path.join(_OUT_DIR, 'realTestImgWithSources.pdf'))
    
    invMap = register.InvariantTriangleMapping()
    ref_inv_uniq, ref_triang_uniq = invMap.generateInvariants(ref_srcs)
    test_inv_uniq, test_triang_uniq = invMap.generateInvariants(test_srcs)

    plotInvariantPoints(ref_inv_uniq, test_inv_uniq, os.path.join(_OUT_DIR, "realInvariants.pdf"))
    
    ref_inv_tree = KDTree(ref_inv_uniq)
    test_inv_tree = KDTree(test_inv_uniq)

    #0.03 is just an empirical number that returns about the same number of matches than inputs
    all_pairs = test_inv_tree.query_ball_tree(ref_inv_tree, 0.01)

    test_inv_matches = []
    ref_inv_matches = []
    for ind1, a_ref_list in enumerate(all_pairs):
        for ind2 in a_ref_list:
            test_inv_matches.append(test_inv_uniq[ind1])
            ref_inv_matches.append(ref_inv_uniq[ind2])        
    test_inv_matches = np.array(test_inv_matches)
    ref_inv_matches = np.array(ref_inv_matches)
    
    plotInvariantPoints(ref_inv_matches, test_inv_matches, os.path.join(_OUT_DIR, "realMatchedInvariants.pdf"))
    
    bestM = register.findAffineTransform(test_srcs, ref_srcs)
    
    plotTransformedSources(ref_srcs, test_srcs, bestM, os.path.join(_OUT_DIR, "realMatchedSources.pdf"))
    
    plotInvariantMaps(os.path.join(_OUT_DIR, "differentInvariantMaps.pdf"))
    
    plotInvariantMap01(os.path.join(_OUT_DIR, "invariantMap01.pdf"))
    