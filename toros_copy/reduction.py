"""@package reduction
    
    Data Reduction module
    ---------------------
    
    A collection of tools to reduce data
    for the Transient Optical Robotic Observatory of the South (TOROS).
    
    Martin Beroiz - 2014
    
    email: <martinberoiz@phys.utb.edu>
    
    University of Texas at San Antonio
    """
